﻿#pragma once

#include <memory>

#include <Ice/Ice.h>

#include "KITProstheticHandInterface.h"

namespace KITProstheticHand
{
    class KITProstheticHandGattlibDriver;
    class KITProstheticHandIceDriver;

    class KITProstheticHandIceServer
    {
    public:
        ~KITProstheticHandIceServer();

        const KITProstheticHandInterfacePtr& driver();


        KITProstheticHandGattlibDriver& lowlevelDriver();

        bool isIceServerRunning() const;

        void startIceServer(
            const std::string& adapterName = "KITProsthesisControlAdapter",
            std::uint64_t port = 10000,
            const std::string& identity = "KITProsthesisControl",
            int argc = 0,
            char* argv[] = {});
        void startIceServer(
            int argc,
            char* argv[],
            const std::string& adapterName = "KITProsthesisControlAdapter",
            std::uint64_t port = 10000,
            const std::string& identity = "KITProsthesisControl");
        void startIceServer(
            const std::string& adapterName,
            const std::string& adapterEndpoint,
            const std::string& identity = "KITProsthesisControl",
            int argc = 0,
            char* argv[] = {});
        void stopIceServer();
        void waitForShutdown();

        void overrideLowlevelDriver(KITProstheticHandGattlibDriver& lldriver);
    private:
        KITProstheticHandInterfacePtr            _driver;
        KITProstheticHandGattlibDriver*          _lowlevelDriver{nullptr};
        Ice::ObjectAdapterPtr                    _adapter;
        std::unique_ptr<Ice::CommunicatorHolder> _iceServer;
    };
}
