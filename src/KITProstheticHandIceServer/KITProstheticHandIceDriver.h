#pragma once

#include <KITProstheticHandGattlibDriver.h>

#include "KITProstheticHandInterface.h"

namespace KITProstheticHand
{
    class KITProstheticHandIceDriver:
        virtual public KITProstheticHandInterface
    {
    private:
        KITProstheticHandGattlibDriver  _driver;
        KITProstheticHandGattlibDriver* _driverPtr;
        bool                            _externalDriver{false};

    public:
        KITProstheticHandIceDriver();
        ~KITProstheticHandIceDriver();

        KITProstheticHandGattlibDriver& driver();

        void overrideLowlevelDriver(KITProstheticHandGattlibDriver& driver);

        // KITProstheticHandInterface interface
    public:
        ConnectionState::State connect(const ConnectionParameters& par = {}, const Ice::Current& = Ice::emptyCurrent) override;
        bool connected(const Ice::Current& = Ice::emptyCurrent) override;
        void disconnect(const Ice::Current& = Ice::emptyCurrent) override;

        void sendGrasp(Ice::Int n, const Ice::Current& = Ice::emptyCurrent) override;
        void sendThumbPWM(const MotorValues& values, const Ice::Current& = Ice::emptyCurrent) override;
        void sendFingersPWM(const MotorValues& values, const Ice::Current& = Ice::emptyCurrent) override;
        void sendRaw(const std::string& raw, const Ice::Current& = Ice::emptyCurrent) override;

        SensorValues getSensorValues(const Ice::Current& = Ice::emptyCurrent) override;
    };
}

