#include <KITProstheticHandIceServer.h>
#include <KITProstheticHandGattlibDriver.h>

int main(int argc, char* argv[])
{
    KITProstheticHand::KITProstheticHandIceServer server;
    KITProstheticHand::ConnectionParameters p;
    p.mac = KITProstheticHand::MAC::V1;
    server.startIceServer(argc, argv);
    server.driver()->connect(p);
    server.waitForShutdown();
    return 0;
}
