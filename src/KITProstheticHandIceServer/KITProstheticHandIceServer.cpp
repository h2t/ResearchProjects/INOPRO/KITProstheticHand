#include "KITProstheticHandIceServer.h"
#include "KITProstheticHandIceDriver.h"

namespace KITProstheticHand
{
    KITProstheticHandIceServer::~KITProstheticHandIceServer()
    {
        stopIceServer();
    }

    const KITProstheticHandInterfacePtr& KITProstheticHandIceServer::driver()
    {
        return _driver;
    }

    KITProstheticHandGattlibDriver &KITProstheticHandIceServer::lowlevelDriver()
    {
        if(_lowlevelDriver)
        {
            return *_lowlevelDriver;
        }
        if(!_driver)
        {
            throw std::logic_error{"No low level driver active"};
        }
        return dynamic_cast<KITProstheticHandIceDriver*>(_driver.get())->driver();
    }

    bool KITProstheticHandIceServer::isIceServerRunning() const
    {
        return _iceServer != nullptr;
    }

    void KITProstheticHandIceServer::startIceServer(
        const std::string& adapterName,
        std::uint64_t port,
        const std::string& identity,
        int argc,
        char *argv[]
    )
    {
        startIceServer(
            adapterName,
            "default -h localhost -p " + std::to_string(port),
            identity,
            argc,
            argv
        );
    }
    void KITProstheticHandIceServer::startIceServer(
        int argc,
        char *argv[],
        const std::string& adapterName,
        std::uint64_t port,
        const std::string& identity
    )
    {
        startIceServer(adapterName, port, identity, argc, argv);
    }

    void KITProstheticHandIceServer::startIceServer(
        const std::string& adapterName,
        const std::string& adapterEndpoint,
        const std::string& identity,
        int argc,
        char *argv[]
    )
    {
        if(isIceServerRunning())
        {
            throw std::logic_error{"Ice server already running"};
        }

        try
        {
            _iceServer .reset(new Ice::CommunicatorHolder(argc, argv));
            _adapter = (*_iceServer)->createObjectAdapterWithEndpoints(adapterName.c_str(), adapterEndpoint.c_str());

            _driver = new KITProstheticHandIceDriver;
            if(_lowlevelDriver)
            {
                dynamic_cast<KITProstheticHandIceDriver*>(_driver.get())->overrideLowlevelDriver(*_lowlevelDriver);
            }

            Ice::ObjectPrx prx = _adapter->add(_driver, Ice::stringToIdentity(identity));
            std::cout << prx->ice_toString() << std::endl;
            _adapter->activate();
        }
        catch (const std::exception&)
        {
            _driver = nullptr;
            _adapter = nullptr;
            _iceServer.reset();
            throw;
        }
    }

    void KITProstheticHandIceServer::stopIceServer()
    {
        if(!isIceServerRunning())
        {
            return;
        }
        _adapter->deactivate();

        (*_iceServer)->shutdown();
        (*_iceServer)->waitForShutdown();

        _driver = nullptr;
        _adapter = nullptr;
        _iceServer.reset();
    }

    void KITProstheticHandIceServer::waitForShutdown()
    {
        if(!isIceServerRunning())
        {
            return;
        }
        (*_iceServer)->waitForShutdown();
    }

    void KITProstheticHandIceServer::overrideLowlevelDriver(KITProstheticHandGattlibDriver& lldriver)
    {
        _lowlevelDriver = &lldriver;
        if(_driver)
        {
            dynamic_cast<KITProstheticHandIceDriver*>(_driver.get())->overrideLowlevelDriver(lldriver);
        }
    }
}
