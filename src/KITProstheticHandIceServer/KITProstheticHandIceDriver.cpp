#include "KITProstheticHandIceDriver.h"

namespace KITProstheticHand
{

KITProstheticHandIceDriver::KITProstheticHandIceDriver() : _driverPtr{&_driver} {}

KITProstheticHandIceDriver::~KITProstheticHandIceDriver()
{
    if(!_externalDriver && connected())
    {
        disconnect();
    }
}

KITProstheticHandGattlibDriver &KITProstheticHandIceDriver::driver()
{
    return *_driverPtr;
}

void KITProstheticHandIceDriver::overrideLowlevelDriver(KITProstheticHandGattlibDriver &driver)
{
    if(_driver.connected())
    {
        throw std::logic_error{"Can't override the driver if the owned driver is connected!"};
    }
    _driverPtr = &driver;
    _externalDriver = true;
}

    ConnectionState::State KITProstheticHandIceDriver::connect(
            const ConnectionParameters& par,
            const Ice::Current&)
    {
        auto p = KITProstheticHandGattlibDriver::SensorProtocol::TposTpwmFposFpwm;
        switch(par.protocol)
        {
        case SensorProtocol::TposTpwmFposFpwm:
            p = KITProstheticHandGattlibDriver::SensorProtocol::TposTpwmFposFpwm;
            break;
        case SensorProtocol::MxPosPwm:
            p = KITProstheticHandGattlibDriver::SensorProtocol::MxPosPwm;
            break;
        }
        const auto r = _driverPtr->connect(
            par.mac,
            p,
            par.adapter,
            par.connectionTimeout,
            std::chrono::microseconds{par.periodUSec}
        );
        using State = KITProstheticHandGattlibDriver::ConnectionState;
        switch(r)
        {
            case State::Connected                            : return ConnectionState::Connected;
            case State::AreadyConnected                      : return ConnectionState::AreadyConnected;

            case State::FailedToFindReadCharacteristic       : return ConnectionState::FailedToFindReadCharacteristic;
            case State::FailedToFindWriteCharacteristic      : return ConnectionState::FailedToFindWriteCharacteristic;
            case State::FailedToFindReadWriteCharacteristics : return ConnectionState::FailedToFindReadWriteCharacteristics;

            case State::FailedToFindDevice                   : return ConnectionState::FailedToFindDevice;
            case State::FailedToConnectDevice                : return ConnectionState::FailedToConnectDevice;

            case State::FailedToOpenAdapter                  : return ConnectionState::FailedToOpenAdapter;
            case State::FailedToScanAdapter                  : return ConnectionState::FailedToScanAdapter;

            case State::ConnectedButNoSensorValues           : return ConnectionState::ConnectedButNoSensorValues;
        }
        throw std::logic_error{"unknown return value from connect"};
    }

    bool KITProstheticHandIceDriver::connected(const Ice::Current&)
    {
        return _driverPtr->connected();
    }

    void KITProstheticHandIceDriver::disconnect(const Ice::Current&)
    {
        _driverPtr->disconnect();
    }

    void KITProstheticHandIceDriver::sendGrasp(Ice::Int n, const Ice::Current&)
    {
        _driverPtr->sendGrasp(static_cast<std::uint64_t>(n));
    }

    void KITProstheticHandIceDriver::sendThumbPWM(const MotorValues& values, const Ice::Current&)
    {
        _driverPtr->sendThumbPWM(
            static_cast<std::uint64_t>(values.v),
            static_cast<std::uint64_t>(values.maxPWM),
            static_cast<std::uint64_t>(values.pos)
        );
    }

    void KITProstheticHandIceDriver::sendFingersPWM(const MotorValues&values, const Ice::Current&)
    {
        _driverPtr->sendFingersPWM(
            static_cast<std::uint64_t>(values.v),
            static_cast<std::uint64_t>(values.maxPWM),
            static_cast<std::uint64_t>(values.pos)
        );
    }

    void KITProstheticHandIceDriver::sendRaw(const std::string&raw, const Ice::Current&)
    {
        _driverPtr->sendRaw(raw);
    }

    SensorValues KITProstheticHandIceDriver::getSensorValues(const Ice::Current&)
    {
        SensorValues value;
        value.thumbPWM  = _driverPtr->getThumbPWM();
        value.thumbPos  = _driverPtr->getThumbPos();

        value.fingersPWM = _driverPtr->getFingersPWM();
        value.fingersPos = _driverPtr->getFingersPos();

        value.IMURoll = _driverPtr->getIMURoll();
        value.IMURoll = _driverPtr->getIMUPitch();
        value.IMURoll = _driverPtr->getIMUYaw();



        return value;
    }

}
