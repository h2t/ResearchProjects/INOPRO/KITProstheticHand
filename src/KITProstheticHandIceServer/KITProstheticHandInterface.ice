#pragma once

module KITProstheticHand
{
    module ConnectionState
    {
        //the enclosing namespace scopes the c-style enum values
        enum State
        {
            Connected,
            AreadyConnected,

            FailedToFindReadCharacteristic,
            FailedToFindWriteCharacteristic,
            FailedToFindReadWriteCharacteristics,

            FailedToFindDevice,
            FailedToConnectDevice,

            FailedToOpenAdapter,
            FailedToScanAdapter,

            ConnectedButNoSensorValues
        };
    };

    module SensorProtocol
    {
        //the enclosing namespace scopes the c-style enum values
        enum Protocol
        {
            TposTpwmFposFpwm,
            MxPosPwm
        };
    };

    struct MotorValues
    {
        long v;
        long maxPWM;
        long pos;
    };

    struct SensorValues
    {
        long thumbPWM;
        long thumbPos;

        long fingersPWM;
        long fingersPos;

        long IMURoll;
        long IMUPitch;
        long IMUYaw;

        long timeOfFlight;
    };

    struct ConnectionParameters
    {
        string mac;
        SensorProtocol::Protocol protocol = SensorProtocol::MxPosPwm;
        string adapter;
        int connectionTimeout = 4;
        long periodUSec = 1000;
    };

    interface KITProstheticHandInterface
    {
        ConnectionState::State connect(ConnectionParameters p);
        bool connected();
        void disconnect();

        void sendGrasp(int n);
        void sendThumbPWM(MotorValues motorValues);
        void sendFingersPWM(MotorValues motorValues);
        void sendRaw(string raw);

        SensorValues getSensorValues();
    };
};
