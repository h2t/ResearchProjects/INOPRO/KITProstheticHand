#pragma once

#include <atomic>
#include <thread>
#include <mutex>
#include <sstream>

#include <bluetooth/sdp.h>

struct _gatt_connection_t;

namespace KITProstheticHand
{
    namespace MAC
    {
        static constexpr auto V1 = "DF:70:E8:81:DB:D6";
        static constexpr auto ARCHES_DLR = "DF:A2:F5:48:E7:50";
        static constexpr auto ARCHES_H2T = "E2:D4:89:F6:1E:1D";
        static constexpr auto V2_2 = "CB:43:34:8F:3C:0A";
        static constexpr auto V2_3 = "C0:59:FC:0F:6B:B5";
    }
}

namespace KITProstheticHand
{
    class KITProstheticHandGattlibDriver
    {
    public:
        enum class SensorProtocol
        {
            TposTpwmFposFpwm,
            MxPosPwm,
            Binary
        };
        enum class ConnectionState
        {
            Connected,
            AreadyConnected,

            FailedToFindReadCharacteristic,
            FailedToFindWriteCharacteristic,
            FailedToFindReadWriteCharacteristics,

            FailedToFindDevice,
            FailedToConnectDevice,

            FailedToOpenAdapter,
            FailedToScanAdapter,

            ConnectedButNoSensorValues
        };

        //management
        ConnectionState connect(const std::string& targ_addr,
                     SensorProtocol prot = SensorProtocol::MxPosPwm,
                     const std::string& adapter_name = "",
                     int connect_timeout = 4,
                     std::chrono::microseconds period = std::chrono::microseconds{1000});
        void disconnect();
        bool connected();
        //sense
        std::int64_t getThumbPWM() const;
        std::int64_t getThumbPos() const;
        std::int64_t getFingersPWM() const;
        std::int64_t getFingersPos() const;
        std::int64_t getIMURoll() const;
        std::int64_t getIMUPitch() const;
        std::int64_t getIMUYaw() const;
        std::int64_t getTimeOfFlight() const;
        //control
        void sendRaw(const std::string raw);
        void sendGrasp(std::uint64_t g);

        void sendThumbPWM(std::uint64_t v, std::uint64_t pwm, std::uint64_t pos);
        void sendFingersPWM(std::uint64_t v, std::uint64_t pwm, std::uint64_t pos);
        void sendPWM(std::uint64_t v, std::uint64_t pwm, std::uint64_t pos, std::uint64_t max_pos, const std::string& name, std::uint64_t motor);

        //control options
        std::uint64_t maxGrasp      = 8;
        std::uint64_t minVelocity   = 10;
        std::uint64_t maxVelocity   = 200;
        std::uint64_t maxPWM        = 2999;
        std::uint64_t maxPosThumb   = 1000000;
        std::uint64_t maxPosFingers = 2000000;
    private:
        //helpers
        std::ostream& cout() const;
        std::ostream& cerr() const;
        bool doConnectDevice();
        bool doDiscoverService();
        void threadReadWriteLoop();
        void parseSensorValueLine(const std::string& line);
        void parseSensorValueBinary(const std::string& line);
        void parseSensorValue(const std::string& data);
        //friends
        friend void notificationCallback(const uuid_t*, const uint8_t*, size_t, void*);
        //management
        std::string                 _targAdr;
        std::thread                 _thread;
        std::atomic<bool>           _threadStop {false};
        std::chrono::microseconds   _threadPeriod{1000};
        //sense
        SensorProtocol              _sensorProtocol;
        std::string                 _readBuffer;
        std::string                 _processBuffer;
        std::atomic_int64_t         _thumbPWM{0};
        std::atomic_int64_t         _thumbPos{0};
        std::atomic_int64_t         _fingersPWM{0};
        std::atomic_int64_t         _fingersPos{0};
        std::atomic_int64_t         _IMUroll{0};
        std::atomic_int64_t         _IMUpitch{0};
        std::atomic_int64_t         _IMUyaw{0};
        std::atomic_int64_t         _timeOfFlight{0};
    private:
        //control
        std::string                 _sendBuffer;
        std::mutex                  _sendBufferMutex;
        //gatt
        void*                       _gattAdapter = nullptr;
        _gatt_connection_t*         _gattConnection = nullptr;
        bool                        _gattCharWriteFound = false;
        bool                        _gattCharReadFound = false;
        bool                        _gattCharReadNotificationActive = false;
        uuid_t                      _gattCharwrite;
        uuid_t                      _gattCharRead;
    };
}
