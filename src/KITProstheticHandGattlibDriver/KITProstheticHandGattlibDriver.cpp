
#include <string>
#include <atomic>
#include <thread>
#include <mutex>
#include <regex>

#include <iomanip>
#include <iostream>

#include <glib.h>

#include <gattlib.h>

#include "KITProstheticHandGattlibDriver.h"

namespace KITProstheticHand
{
    static const std::string uuid_uart_service = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
    static const std::string uuid_write_characteristic = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    static const std::string uuid_read_characteristic = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";

    static thread_local bool  ble_discovered_device_callback_found = false;
    static thread_local const std::string* ble_discovered_device_callback_adr = nullptr;

    void notificationCallback(
        const uuid_t*,
        const uint8_t*  data,
        size_t          data_length,
        void*           connection_ptr
    )
    {
        _gatt_connection_t* conn = reinterpret_cast<_gatt_connection_t*>(connection_ptr);
        auto obj = reinterpret_cast<KITProstheticHandGattlibDriver*>(conn->notification_user_data);
        const char* cdata = reinterpret_cast<const char*>(data);

        obj->_readBuffer.reserve(obj->_readBuffer.size() + data_length);
        obj->_readBuffer.insert(obj->_readBuffer.end(), cdata, cdata + data_length);

        /*std::size_t last = 0;
        for (
            auto idx = obj->_readBuffer.find_first_of("\n", last);
            idx != std::string::npos;
            idx = obj->_readBuffer.find_first_of("\n", last)
        )
        {
            const auto line = obj->_readBuffer.substr(last, idx - last - 1);
            obj->parseSensorValueLine(line);
            last = idx + 1;
        }
        obj->_readBuffer = obj->_readBuffer.substr(last);*/

        const auto readData = obj->_readBuffer;
        obj->parseSensorValue(readData);
        obj->_readBuffer.clear();
    }

    void ble_discovered_device_callback(const char *addr, const char *name)
    {
        const auto& targ_adr = *ble_discovered_device_callback_adr;
        if(ble_discovered_device_callback_found)
        {
            std::cout << '[' << targ_adr << "] SKIPPING " << addr << " since the target device was found\n";
            return;
        }
        ble_discovered_device_callback_found = targ_adr == addr;
        if(!ble_discovered_device_callback_found)
        {
            std::cout << '[' << targ_adr << "] SKIPPING " << addr
                      << " since it does not match the target address\n";
            return;
        }
        std::cout << '[' << targ_adr << "] FOUND device '" << name << "'\n";
    }


    KITProstheticHandGattlibDriver::ConnectionState KITProstheticHandGattlibDriver::connect(
        const std::string &targ_addr,
        SensorProtocol prot,
        const std::string &adapter_name,
        int connect_timeout,
        std::chrono::microseconds thread_period)
    {
        if(connected())
        {
            return ConnectionState::AreadyConnected;
        }
        ble_discovered_device_callback_found = false;
        ble_discovered_device_callback_adr = &targ_addr;
        _targAdr = targ_addr;
        _sensorProtocol = prot;
        {
            const char* adapter_name_ptr = nullptr;
            if(!adapter_name.empty())
            {
                adapter_name_ptr = adapter_name.c_str();
            }
            if (gattlib_adapter_open(adapter_name_ptr, &_gattAdapter))
            {
                cerr() << "Failed to open adapter '" << adapter_name << "'\n";
                return ConnectionState::FailedToOpenAdapter;
            }
            cout() << "start scanning on adapter '" << adapter_name << "'\n";
            if (gattlib_adapter_scan_enable(_gattAdapter, &ble_discovered_device_callback, connect_timeout))
            {
                cerr() << "Failed to scan on adapter '" << adapter_name << "'\n";
                return ConnectionState::FailedToScanAdapter;
            }
            gattlib_adapter_scan_disable(_gattAdapter);
            cout() << "done scanning on adapter '" << adapter_name << "'\n";
        }
        if(!ble_discovered_device_callback_found)
        {
            return ConnectionState::FailedToFindDevice;
        }
        if(!doConnectDevice())
        {
            return ConnectionState::FailedToConnectDevice;
        }
        if(!doDiscoverService())
        {
            if(!_gattCharReadFound && !_gattCharWriteFound)
            {
                return ConnectionState::FailedToFindReadWriteCharacteristics;
            }
            if(!_gattCharReadFound)
            {
                return ConnectionState::FailedToFindReadCharacteristic;
            }
            if(!_gattCharWriteFound)
            {
                return ConnectionState::FailedToFindWriteCharacteristic;
            }
        }

        _readBuffer.clear();
        _processBuffer.clear();
        gattlib_register_notification(_gattConnection, notificationCallback, this);
        _gattCharReadNotificationActive = !gattlib_notification_start(_gattConnection, &_gattCharRead);

        _threadPeriod = thread_period;
        _threadStop = false;
        _thread = std::thread
        {
            [this]
            {
                threadReadWriteLoop();
            }
        };

        if(!_gattCharReadNotificationActive)
        {
            cerr() << "failed to start notifications for the read characteristic!"
                   << "No sensor values will be available\n";
            return ConnectionState::ConnectedButNoSensorValues;
        }
        return ConnectionState::Connected;
    }

    void KITProstheticHandGattlibDriver::disconnect()
    {
        if(!_thread.joinable())
        {
            return;
        }
        _threadStop = true;
        _thread.join();
        if(_gattCharReadNotificationActive)
        {
            gattlib_notification_stop(_gattConnection, &_gattCharRead);
            _gattCharReadNotificationActive = false;
        }
        if(_gattConnection)
        {
            gattlib_disconnect(_gattConnection);
            _gattConnection = nullptr;
        }
        if(_gattAdapter)
        {
            gattlib_adapter_close(_gattAdapter);
            _gattAdapter = nullptr;
        }
        cout() << "data still in the read buffer:\n" << _readBuffer << '\n';
        _readBuffer.clear();
        cout() << "disconnecting\n";
    }

    bool KITProstheticHandGattlibDriver::connected()
    {
        return _thread.joinable();
    }

    int64_t KITProstheticHandGattlibDriver::getThumbPWM() const
    {
        return _thumbPWM;
    }

    int64_t KITProstheticHandGattlibDriver::getThumbPos() const
    {
        return _thumbPos;
    }

    int64_t KITProstheticHandGattlibDriver::getFingersPWM() const
    {
        return _fingersPWM;
    }

    int64_t KITProstheticHandGattlibDriver::getFingersPos() const
    {
        return _fingersPos;
    }

    int64_t KITProstheticHandGattlibDriver::getIMURoll() const
    {
        return _IMUroll;
    }

    int64_t KITProstheticHandGattlibDriver::getIMUPitch() const
    {
        return _IMUpitch;
    }

    int64_t KITProstheticHandGattlibDriver::getIMUYaw() const
    {
        return _IMUyaw;
    }

    int64_t KITProstheticHandGattlibDriver::getTimeOfFlight() const
    {
        return _timeOfFlight;
    }

    void KITProstheticHandGattlibDriver::sendRaw(const std::string raw)
    {
        do
        {
            std::lock_guard<std::mutex> g {_sendBufferMutex};
            if(_sendBuffer.empty())
            {
                _sendBuffer = raw;
                break;
            }
        }
        while(true);
    }

    void KITProstheticHandGattlibDriver::sendGrasp(uint64_t g)
    {
        if (g > maxGrasp)
        {
            throw std::invalid_argument
            {
                "KITProstheticHandGattlibDriver::send_grasp( g = " + std::to_string(g) +
                        " ): the maximal value for g is " + std::to_string(maxGrasp)
            };
        }
        sendRaw("g" + std::to_string(g) + "\n");
    }

    void KITProstheticHandGattlibDriver::sendThumbPWM(uint64_t v, uint64_t pwm, uint64_t pos)
    {
        static const std::string name = "send thumb PWM";
        sendPWM(v,  pwm, pos, maxPosFingers, name, 2);
    }

    void KITProstheticHandGattlibDriver::sendFingersPWM(uint64_t v, uint64_t pwm, uint64_t pos)
    {
        static const std::string name = "send fingers PWM";
        sendPWM(v,  pwm, pos, maxPosFingers, name, 3);
    }

    void KITProstheticHandGattlibDriver::sendPWM(uint64_t v, uint64_t pwm, uint64_t pos, uint64_t max_pos, const std::string &name, uint64_t motor)
    {
        if (v < minVelocity || v > maxVelocity)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for v is [" + std::to_string(minVelocity) +
                    ", " + std::to_string(maxVelocity) + "]"
                };
        }
        if (pwm > maxPWM)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for pwm is [0, , " +
                    std::to_string(maxPWM) + "]"
                };
        }
        if (pos > max_pos)
        {
            throw std::invalid_argument
                {
                    name + "( v = " + std::to_string(v) +
                    " , pwm = " + std::to_string(pwm) +
                    " , pos = " + std::to_string(pos) +
                    " ): The interval for pos is [0, " +
                    std::to_string(max_pos) + "]"
                };
        }
        std::stringstream str;
        str << 'M' << motor << ',' << v << ',' << pwm << ',' << pos << '\n';
        sendRaw(str.str());
    }

    std::ostream& KITProstheticHandGattlibDriver::cout() const
    {
        return std::cout << '[' << _targAdr << "] ";
    }

    std::ostream& KITProstheticHandGattlibDriver::cerr() const
    {
        return std::cerr << '[' << _targAdr << "] ERROR: ";
    }

    bool KITProstheticHandGattlibDriver::doConnectDevice()
    {
        const char* addr = _targAdr.c_str();

        cout() << "connecting (first trying address type random then public)\n";

        _gattConnection = gattlib_connect(nullptr, addr, BDADDR_LE_RANDOM, BT_SEC_LOW, 0, 0);
        if (_gattConnection)
        {
            cout() << "Succeeded to connect to the bluetooth device with random address.\n";
            return true;
        }

        _gattConnection = gattlib_connect(nullptr, addr, BDADDR_LE_PUBLIC, BT_SEC_LOW, 0, 0);
        if (_gattConnection)
        {
            cout() << "Succeeded to connect to the bluetooth device.\n";
            return true;
        }

        cerr() << "Fail to connect to the bluetooth device.\n";
        return false;
    }

    bool KITProstheticHandGattlibDriver::doDiscoverService()
    {
        _gattCharWriteFound = false;
        _gattCharReadFound = false;
        char uuid_str[MAX_LEN_UUID_STR + 1];

    #define ohex(...)    std::hex << std::setw(8) << std::setfill('0') << __VA_ARGS__ <<  std::dec << std::setw(0) << std::setfill(' ')
    #define odec(i, ...) std::dec << std::setw(i) << std::setfill('0') << __VA_ARGS__ <<  std::dec << std::setw(0) << std::setfill(' ')
        //characteristics
        {
            gattlib_characteristic_t* characteristics;
            int characteristics_count;
            if (gattlib_discover_char(_gattConnection, &characteristics, &characteristics_count))
            {
                cerr() << "Fail to discover characteristics.\n";
                return false;
            }

            cout() << "searching r/w characteristics\n";
            for (int i = 0; i < characteristics_count; i++)
            {
                gattlib_uuid_to_string(&characteristics[i].uuid, uuid_str, sizeof(uuid_str));

                cout() << "    characteristic[" << odec(3,i)
                       << "] properties:0x" << ohex(static_cast<unsigned>(characteristics[i].properties))
                       << " value_handle:0x" << ohex(characteristics[i].value_handle)
                       << " uuid:" << uuid_str << '\n';
                if(uuid_str == uuid_read_characteristic)
                {
                    cout() << "        this is the read characteristic!\n";
                    _gattCharReadFound = true;
                    _gattCharRead = characteristics[i].uuid;
                }
                if(uuid_str == uuid_write_characteristic)
                {
                    cout() << "        this is the write characteristic!\n";
                    _gattCharWriteFound = true;
                    _gattCharwrite = characteristics[i].uuid;
                }
                if(_gattCharReadFound && _gattCharWriteFound)
                {
                    break;
                }

            }
            free(characteristics);
            if(!_gattCharReadFound && !_gattCharWriteFound)
            {
                cerr() << "read and write characteristic '" << uuid_read_characteristic << "' not found\n";
                return false;
            }
            if(!_gattCharReadFound)
            {
                cerr() << "read characteristic '" << uuid_read_characteristic << "' not found\n";
                return false;
            }
            if(!_gattCharWriteFound)
            {
                cerr() << "write characteristic '" << uuid_write_characteristic << "' not found\n";
                return false;
            }
        }
        return true;
    #undef ohex
    #undef odec
    }

    void KITProstheticHandGattlibDriver::threadReadWriteLoop()
    {
        if(!_gattCharWriteFound || !_gattCharReadFound)
        {
            cerr() << "Either write or read characteristic were not found!\n";
            return;
        }

        std::string send_buffer;
        std::int64_t t_read = 0;
        std::int64_t t_write = 0;
        std::int64_t t_sleep = 0;
        std::uint64_t n_iterations = 0;

        cout() << "starting the read/write loop\n";
        const auto lbeg = std::chrono::high_resolution_clock::now();
        while(!_threadStop)
        {
            const auto ibeg = std::chrono::high_resolution_clock::now();
            ++n_iterations;
            //handle read notifications
            {
                const auto rbeg = std::chrono::high_resolution_clock::now();
                g_main_context_iteration(nullptr, false);
                const auto rend = std::chrono::high_resolution_clock::now();
                t_read += std::chrono::duration_cast<std::chrono::nanoseconds>(rend-rbeg).count();
            }
            //write
            {
                {
                    std::lock_guard<std::mutex> g {_sendBufferMutex};
                    std::swap(_sendBuffer, send_buffer);
                }
                if(!send_buffer.empty())
                {
                    cout() << "sending '" << send_buffer << "'\n";
                    const auto wbeg = std::chrono::high_resolution_clock::now();
                    gattlib_write_char_by_uuid(_gattConnection, &_gattCharwrite, send_buffer.data(), send_buffer.size());
                    const auto wend = std::chrono::high_resolution_clock::now();
                    t_write += std::chrono::duration_cast<std::chrono::nanoseconds>(wend-wbeg).count();
                    send_buffer.clear();
                }
            }
           //sleep
            {
                const auto sbeg = std::chrono::high_resolution_clock::now();
                std::this_thread::sleep_until(ibeg + _threadPeriod);
                const auto send = std::chrono::high_resolution_clock::now();
                t_sleep += std::chrono::duration_cast<std::chrono::nanoseconds>(send-sbeg).count();
            }

        }
        const auto lend = std::chrono::high_resolution_clock::now();
        const std::int64_t t_loop = std::chrono::duration_cast<std::chrono::nanoseconds>(lend-lbeg).count();
        const std::int64_t t_other = t_loop - t_read - t_write - t_sleep;

        const double t_loop_ms  = t_loop  / 1000000.0;
        const double t_read_ms  = t_read  / 1000000.0;
        const double t_write_ms = t_write / 1000000.0;
        const double t_sleep_ms = t_sleep / 1000000.0;
        const double t_other_ms = t_other / 1000000.0;

        cout() << "exiting the read/write loop\n";
        cout() << "time r/w loop        = " << t_loop_ms  << " ms (" << t_loop_ms  / n_iterations << " ms/iter)\n";
        cout() << "time gattlib_read    = " << t_read_ms  << " ms (" << t_read_ms  / n_iterations << " ms/iter)\t-> " << 100.0 * t_read  / t_loop << "%\n";
        cout() << "time gattlib_write   = " << t_write_ms << " ms (" << t_write_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_write / t_loop << "%\n";
        cout() << "time sleep           = " << t_sleep_ms << " ms (" << t_sleep_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_sleep / t_loop << "%\n";
        cout() << "time other           = " << t_other_ms << " ms (" << t_other_ms / n_iterations << " ms/iter)\t-> " << 100.0 * t_other / t_loop << "%\n";
        cout() << "number of iterations = " << n_iterations << '\n';
    }

    void KITProstheticHandGattlibDriver::parseSensorValueLine(const std::string& line)
    {
        #define end   R"([ \t\n\r\x00]*$)"
        #define start R"(^[ \t\x00]*)"
        #define white R"([ \t]+)"
        #define num   R"(([-+]?[1-9][0-9]*|0))"

        const auto print_unknown = [&](const char* protocolname)
        {
            std::stringstream s;
            s << std::hex << std::setw(2) << std::setfill('0');
            for(char c : line)
            {
                s << std::hex << static_cast<std::int16_t>(c) << ' ';
            }

            cerr() << "[protocol " << protocolname << "] unknown sensor data '" << line << "' (len = " << line.size() << ", hex = " << s.str() << ")\n";
        };

        //skip empty strings
        {
            static const std::regex regex {start end};
            std::smatch match;

            if (std::regex_match(line, match, regex))
            {
                return;
            }
        }

        if(_sensorProtocol == SensorProtocol::TposTpwmFposFpwm)
        {
            static const std::regex regex {start num white num white num white num end};
            std::smatch match;
            if (std::regex_match(line, match, regex))
            {
                _thumbPos = std::stoll(match[1]);
                _thumbPWM = std::stoll(match[2]);
                _fingersPos = std::stoll(match[3]);
                _fingersPWM = std::stoll(match[4]);
            }
            else
            {
                print_unknown("TposTpwmFposFpwm");
            }
        }
        else if(_sensorProtocol == SensorProtocol::MxPosPwm)
        {


            static const std::regex regexm2 {start "M2:" white "Pos.:" white num white "PWM:" white num end};
            static const std::regex regexm3 {start "M3:" white "Pos.:" white num white "PWM:" white num end};


            std::smatch match;
            if (std::regex_match(line, match, regexm2))
            {
                _thumbPos = std::stoll(match[1]);
                _thumbPWM = std::stoll(match[2]);
            }
            else if (std::regex_match(line, match, regexm3))
            {
                _fingersPos = std::stoll(match[1]);
                _fingersPWM = std::stoll(match[2]);
            }
            else
            {
                print_unknown("MxPosPwm");
            }
        }
        else
        {
            throw std::logic_error {"unhandled protocol version"};
        }
        #undef end
        #undef start
        #undef white
        #undef num
    }

    void KITProstheticHandGattlibDriver::parseSensorValueBinary(const std::string& line)
    {
        // get a variable buffer to build the data into
        long long buffer{0};

        // position of finger motor
        buffer = line[0] << 24;
        buffer |= line[1] << 16;
        buffer |= line[2] << 8;
        buffer |= line[3];

        _fingersPos = buffer;

        // PWM of finger motor
        buffer = line[4] << 24;
        buffer |= line[5] << 16;
        buffer |= line[6] << 8;
        buffer |= line[7];

        _fingersPWM = buffer;

        // position of thumb motor
        buffer = line[8] << 24;
        buffer |= line[9] << 16;
        buffer |= line[10] << 8;
        buffer |= line[11];

        _thumbPos = buffer;

        // PWM of thumb motor
        buffer = line[12] << 24;
        buffer |= line[13] << 16;
        buffer |= line[14] << 8;
        buffer |= line[15];

        _thumbPWM = buffer;

        // IMU roll angle
        buffer = line[16] << 8;
        buffer |= line[17];

        _IMUroll = buffer;

        // IMU pitch angle
        buffer = line[18] << 8;
        buffer |= line[19];

        _IMUpitch = buffer;

        // IMU yaw angle
        buffer = line[20] << 8;
        buffer |= line[21];

        _IMUyaw = buffer;

        // distance sensor
        buffer = line[22] << 8;
        buffer |= line[23];

        _timeOfFlight = buffer;
    }

    void KITProstheticHandGattlibDriver::parseSensorValue(const std::string& data)
    {
        #define startSequenceByte   (char)0b10101010

        _processBuffer.append(data);
        std::size_t last = 0;
        char startSequence[8] = {startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte,startSequenceByte};

        switch(_sensorProtocol)
        {
            case SensorProtocol::TposTpwmFposFpwm:
            case SensorProtocol::MxPosPwm:
                for (
                        auto idx = _processBuffer.find_first_of("\n", last);
                        idx != std::string::npos;
                        idx = _processBuffer.find_first_of("\n", last)
                    )
                    {
                        const auto line = _processBuffer.substr(last, idx - last - 1);
                        parseSensorValueLine(line);
                        last = idx + 1;
                    }
                _processBuffer = _processBuffer.substr(last);
                break;
            case SensorProtocol::Binary:
                for (
                    auto idx = _processBuffer.find(startSequence, last);
                    idx != std::string::npos;
                    idx = _processBuffer.find(startSequence, last)
                )
                {
                    // full stream is there
                    if(_processBuffer.size() - idx >= 32)
                    {
                        const auto line = _processBuffer.substr(idx + 8, 24);
                        parseSensorValueBinary(line);
                        last = idx + 32;
                    }
                    // discard what we had before
                    else
                    {
                        last = idx;
                        break;
                    }
                }
                _processBuffer = _processBuffer.substr(last);
                break;
            default:
                throw std::logic_error {"unhandled protocol version"};
        }

        #undef startSequenceByte
    }
}
