#include <iostream>
#include <fstream>
#include <chrono>

#include "KITProstheticHandGattlibDriver.h"

void read_and_wait(const KITProstheticHand::KITProstheticHandGattlibDriver& driver, unsigned ds)
{
    while(ds--)
    {
        std::cout << "tpos " << driver.getThumbPos() << " tpwm " << driver.getThumbPWM() << " fpos " << driver.getFingersPos() << " fpwm " << driver.getFingersPWM() << '\n';
        std::this_thread::sleep_for(std::chrono::milliseconds{100});
    }
}

int main()
{
    KITProstheticHand::KITProstheticHandGattlibDriver driver;
    if(driver.connect(KITProstheticHand::MAC::V2_2, KITProstheticHand::KITProstheticHandGattlibDriver::SensorProtocol::Binary) != KITProstheticHand::KITProstheticHandGattlibDriver::ConnectionState::Connected)
    {
        return 1;
    }

    std::ofstream datalog("datalog.csv", std::ios::ate);
    //datalog.open ("datalog.csv", std::ios::ate);
    datalog << "thumbPos, thumbPWM, fingerPos, fingerPWM, IMURoll, IMUPitch, IMUYaw, TOF U16Subindex0, TOF depth1, TOF depth2, TOF depth3, TOF depth4, TOF num, semiautonomous state, recognized object\n";
    datalog.flush();
    //datalog.close();

    while(1)
    {
        read_and_wait(driver, 2);
    }
    return 0;
}
