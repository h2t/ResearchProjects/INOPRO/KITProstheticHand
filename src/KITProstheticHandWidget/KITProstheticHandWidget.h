#pragma once

#include <QWidget>

#include <KITProstheticHandGattlibDriver.h>
#ifdef KITProstheticHandWidget_WITH_ICE
#include <KITProstheticHandIceServer.h>
#endif

namespace Ui
{
    class KITProstheticHandWidget;
}

namespace KITProstheticHand
{

    class KITProstheticHandWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit KITProstheticHandWidget(QWidget *parent = nullptr);
        ~KITProstheticHandWidget();

    protected:
        void timerEvent(QTimerEvent* event) override;

    private slots:
        void on_pushButtonSendRaw_released();
        void on_pushButtonConnect_released();
        void on_pushButtonSendGrasp_released();
        void on_pushButtonSendThumb_released();
        void on_pushButtonSendFingers_released();
        void on_pushButtonIceConnect_released();
        void on_pushButtonDisconnect_released();
        void on_pushButtonIceDisconnect_released();

    private:
        Ui::KITProstheticHandWidget *ui;
        #ifdef KITProstheticHandWidget_WITH_ICE
        KITProstheticHandIceServer _server;
        #endif
        KITProstheticHandGattlibDriver _driver;
    };
}
