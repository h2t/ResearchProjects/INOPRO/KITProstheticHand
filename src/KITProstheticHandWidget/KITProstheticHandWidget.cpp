#include "KITProstheticHandWidget.h"
#include "ui_KITProstheticHandWidget.h"

namespace KITProstheticHand
{
    KITProstheticHandWidget::KITProstheticHandWidget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::KITProstheticHandWidget)
    {
        ui->setupUi(this);
    #ifdef KITProstheticHandWidget_WITH_ICE
        _server.overrideLowlevelDriver(_driver);
    #else
        ui->groupBoxIce->setHidden(true);
        ui->groupBoxIce->setDisabled(true);
    #endif

        connect(ui->pushButtonSendRaw      , SIGNAL(released()), this, SLOT(on_pushButtonSendRaw_released()));
        connect(ui->pushButtonConnect      , SIGNAL(released()), this, SLOT(on_pushButtonConnect_released()));
        connect(ui->pushButtonSendGrasp    , SIGNAL(released()), this, SLOT(on_pushButtonSendGrasp_released()));
        connect(ui->pushButtonSendThumb    , SIGNAL(released()), this, SLOT(on_pushButtonSendThumb_released()));
        connect(ui->pushButtonIceConnect   , SIGNAL(released()), this, SLOT(on_pushButtonIceConnect_released()));
        connect(ui->pushButtonDisconnect   , SIGNAL(released()), this, SLOT(on_pushButtonDisconnect_released()));
        connect(ui->pushButtonSendFingers  , SIGNAL(released()), this, SLOT(on_pushButtonSendFingers_released()));
        connect(ui->pushButtonIceDisconnect, SIGNAL(released()), this, SLOT(on_pushButtonIceDisconnect_released()));

        //limits
        ui->spinBoxThumbPWM->setMaximum(static_cast<int>(_driver.maxPWM));
        ui->spinBoxThumbPWM->setValue(static_cast<int>(_driver.maxPWM) / 2);

        ui->spinBoxThumbPos->setMaximum(static_cast<int>(_driver.maxPosThumb));
        ui->spinBoxThumbPos->setValue(static_cast<int>(_driver.maxPosThumb) / 2);
        ui->spinBoxThumbPos->setSingleStep(1000);

        ui->spinBoxThumbVel->setMinimum(static_cast<int>(_driver.minVelocity));
        ui->spinBoxThumbVel->setMaximum(static_cast<int>(_driver.maxVelocity));
        ui->spinBoxThumbVel->setValue(static_cast<int>((_driver.maxVelocity - _driver.minVelocity) / 2 + _driver.minVelocity));

        ui->spinBoxFingersPWM->setMaximum(static_cast<int>(_driver.maxPWM));
        ui->spinBoxFingersPWM->setValue(static_cast<int>(_driver.maxPWM) / 2);

        ui->spinBoxFingersPos->setMaximum(static_cast<int>(_driver.maxPosFingers));
        ui->spinBoxFingersPos->setValue(static_cast<int>(_driver.maxPosFingers) / 2);
        ui->spinBoxFingersPos->setSingleStep(1000);

        ui->spinBoxFingersVel->setMinimum(static_cast<int>(_driver.minVelocity));
        ui->spinBoxFingersVel->setMaximum(static_cast<int>(_driver.maxVelocity));
        ui->spinBoxFingersVel->setValue(static_cast<int>((_driver.maxVelocity - _driver.minVelocity) / 2 + _driver.minVelocity));

        startTimer(100);
    }

    KITProstheticHandWidget::~KITProstheticHandWidget()
    {
#ifdef KITProstheticHandWidget_WITH_ICE
        _server.stopIceServer();
#endif
        _driver.disconnect();
        delete ui;
    }

    void KITProstheticHandWidget::timerEvent(QTimerEvent*)
    {
        ui->labelThumbPWM->setText(QString::number(_driver.getThumbPWM()));
        ui->labelThumbPos->setText(QString::number(_driver.getThumbPos()));
        ui->labelFingersPWM->setText(QString::number(_driver.getFingersPWM()));
        ui->labelFingersPos->setText(QString::number(_driver.getFingersPos()));
        ui->labelIMURoll->setText(QString::number(_driver.getIMURoll()));
        ui->labelIMUPitch->setText(QString::number(_driver.getIMUPitch()));
        ui->labelIMUYaw->setText(QString::number(_driver.getIMUYaw()));
        ui->labelTimeOfFlight->setText(QString::number(_driver.getTimeOfFlight()));
    }

    void KITProstheticHandWidget::on_pushButtonConnect_released()
    {
        const std::string addr = ui->comboBoxMacAddress->currentText().trimmed().toStdString().substr(0, 17);
        const std::string adapter = ui->lineEditAdapter->text().toStdString();
        const auto p = static_cast<KITProstheticHandGattlibDriver::SensorProtocol>(ui->comboBoxProtocol->currentIndex());
        const auto r = _driver.connect(
            addr,
            p,
            adapter == "auto" ? "" : adapter,
            ui->spinBoxConnectionTimeout->value(),
            std::chrono::microseconds{ui->spinBoxHandlerPeriod->value()}
        );
        bool connected = false;

        switch(r)
        {
        case KITProstheticHandGattlibDriver::ConnectionState::Connected:
            connected = true;
            ui->labelStatus->setText("Connected");
            break;
        case KITProstheticHandGattlibDriver::ConnectionState::AreadyConnected:
            connected = true;
            ui->labelStatus->setText("Connected - AlreadyConnected");
            break;

        case KITProstheticHandGattlibDriver::ConnectionState::FailedToFindReadCharacteristic:
            ui->labelStatus->setText("Disconnected - FailedToFindReadCharacteristic");
            break;
        case KITProstheticHandGattlibDriver::ConnectionState::FailedToFindWriteCharacteristic:
            ui->labelStatus->setText("Disconnected - FailedToFindWriteCharacteristic");
            break;
        case KITProstheticHandGattlibDriver::ConnectionState::FailedToFindReadWriteCharacteristics:
            ui->labelStatus->setText("Disconnected - FailedToFindReadWriteCharacteristics");
            break;

        case KITProstheticHandGattlibDriver::ConnectionState::FailedToFindDevice:
            ui->labelStatus->setText("Disconnected - FailedToFindDevice");
            break;
        case KITProstheticHandGattlibDriver::ConnectionState::FailedToConnectDevice:
            ui->labelStatus->setText("Disconnected - FailedToConnectDevice");
            break;

        case KITProstheticHandGattlibDriver::ConnectionState::FailedToOpenAdapter:
            ui->labelStatus->setText("Disconnected - FailedToOpenAdapter");
            break;
        case KITProstheticHandGattlibDriver::ConnectionState::FailedToScanAdapter:
            ui->labelStatus->setText("Disconnected - FailedToScanAdapter");
            break;

        case KITProstheticHandGattlibDriver::ConnectionState::ConnectedButNoSensorValues:
            connected = true;
            ui->labelStatus->setText("Connected - ConnectedButNoSensorValues");
            break;
        }

        if(connected)
        {
            ui->pushButtonConnect->setDisabled(true);
            ui->pushButtonDisconnect->setEnabled(true);
            ui->widgetSensors->setEnabled(true);
            ui->widgetControl->setEnabled(true);

            ui->comboBoxMacAddress->setDisabled(true);
            ui->lineEditAdapter->setDisabled(true);
            ui->comboBoxProtocol->setDisabled(true);
            ui->spinBoxConnectionTimeout->setDisabled(true);
            ui->spinBoxHandlerPeriod->setDisabled(true);
        }
    }

    void KITProstheticHandWidget::on_pushButtonDisconnect_released()
    {
        _driver.disconnect();
        ui->pushButtonConnect->setEnabled(true);
        ui->pushButtonDisconnect->setDisabled(true);
        ui->widgetSensors->setDisabled(true);
        ui->widgetControl->setDisabled(true);

        ui->comboBoxMacAddress->setEnabled(true);
        ui->lineEditAdapter->setEnabled(true);
        ui->comboBoxProtocol->setEnabled(true);
        ui->spinBoxConnectionTimeout->setEnabled(true);
        ui->spinBoxHandlerPeriod->setEnabled(true);

        ui->labelStatus->setText("Disconnected");
    }

    void KITProstheticHandWidget::on_pushButtonIceConnect_released()
    {
        #ifdef KITProstheticHandWidget_WITH_ICE
        _server.startIceServer(
            ui->lineEditAdapterName->text().toStdString(),
            static_cast<std::uint64_t>(ui->spinBoxIcePort->value()),
            ui->lineEditIceIdentity->text().toStdString()
        );
        #endif
        ui->pushButtonIceConnect->setDisabled(true);
        ui->pushButtonIceDisconnect->setEnabled(true);
    }

    void KITProstheticHandWidget::on_pushButtonIceDisconnect_released()
    {
        #ifdef KITProstheticHandWidget_WITH_ICE
        _server.stopIceServer();
        #endif
        ui->pushButtonIceConnect->setEnabled(true);
        ui->pushButtonIceDisconnect->setDisabled(true);
    }

    void KITProstheticHandWidget::on_pushButtonSendThumb_released()
    {
        _driver.sendThumbPWM(
            static_cast<std::uint64_t>(ui->spinBoxThumbVel->value()),
            static_cast<std::uint64_t>(ui->spinBoxThumbPWM->value()),
            static_cast<std::uint64_t>(ui->spinBoxThumbPos->value())
        );
    }

    void KITProstheticHandWidget::on_pushButtonSendFingers_released()
    {
        _driver.sendFingersPWM(
            static_cast<std::uint64_t>(ui->spinBoxFingersVel->value()),
            static_cast<std::uint64_t>(ui->spinBoxFingersPWM->value()),
            static_cast<std::uint64_t>(ui->spinBoxFingersPos->value())
        );
    }

    void KITProstheticHandWidget::on_pushButtonSendGrasp_released()
    {
        _driver.sendGrasp(static_cast<std::uint64_t>(ui->comboBoxGrasp->currentIndex()));
    }

    void KITProstheticHandWidget::on_pushButtonSendRaw_released()
    {
        _driver.sendRaw(ui->lineEditCommandRaw->text().toStdString() + "\n");
    }
}
